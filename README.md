# Corporate Blue

Website theme based on template designed by [Yane Naumoski](http://www.pcklab.com/).
Created by [Andrey Grachev](http://cs.angra.pw/)

[Demo](http://cs.angra.pw/templates/corporateblue/)

## License

Use it freely but do not distribute, sell or reupload elsewhere.